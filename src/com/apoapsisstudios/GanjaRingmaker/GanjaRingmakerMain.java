package com.apoapsisstudios.GanjaRingmaker;

import java.awt.Graphics2D;

import org.dreambot.api.methods.Calculations;
import org.dreambot.api.methods.magic.Normal;
import org.dreambot.api.methods.map.Area;
import org.dreambot.api.methods.tabs.Tab;
import org.dreambot.api.script.AbstractScript;
import org.dreambot.api.script.Category;
import org.dreambot.api.script.ScriptManifest;
import org.dreambot.api.utilities.Timer;
import org.dreambot.api.wrappers.interactive.GameObject;
import org.dreambot.api.wrappers.items.Item;

@ScriptManifest(author = "tolgaustunkok", category = Category.MONEYMAKING, name = "Ganja Ringmaker", version = 0.628)
public class GanjaRingmakerMain extends AbstractScript
{
	private boolean isStarted;
	private BotStates currentState;
	private Area bankArea;
	private Area furnaceArea;
	private Timer timer;
	private BotGUI botGUI;
	private boolean canDraw = false;

	@Override
	public void onStart()
	{
		isStarted = false;
		bankArea = new Area(3098, 3494, 3095, 3496, 0);
		furnaceArea = new Area(3109, 3499, 3107, 3498, 0);
		botGUI = new BotGUI(this, "Ganja Ringmaker");
		currentState = BotStates.START;
	}

	@Override
	public int onLoop()
	{
		switch (currentState)
		{
		case START:
			log("START state.");
			if (isStarted)
			{
				timer = new Timer();
				botGUI.Display();
				canDraw = true;
				currentState = BotStates.WALK_TO_BANK;
			}
			else
			{
				currentState = BotStates.START;
				isStarted = true;
			}
			break;
		case WALK_TO_BANK:
			log("WALK_TO_BANK state.");
			if (bankArea.contains(getLocalPlayer()))
				currentState = BotStates.EMPTY_RINGS;
			else
				Utilities.GoToArea(this, bankArea);
			break;
		case EMPTY_RINGS:
			log("EMPTY_RINGS state.");
			if (Utilities.OpenBank(this))
			{
				Utilities.DepositAll(this, "Sapphire ring");
				currentState = BotStates.CHECK;
			}
			break;
		case CHECK:
			log("CHECK state.");
			if (getBank().contains("Sapphire") && getBank().contains("Gold bar"))
			{
				currentState = BotStates.TAKE_GOLD_SAPPHIRE;
			}
			else
			{
				currentState = BotStates.BANK_MOULD_AND_RINGS;
			}
			break;
		case TAKE_GOLD_SAPPHIRE:
			log("TAKE_GOLD_SAPPHIRE state.");
			if (Utilities.OpenBank(this))
			{
				if (Utilities.Withdraw(this, "Gold bar", 13))
				{
					if (Utilities.Withdraw(this, "Sapphire", 13))
					{
						currentState = BotStates.WALK_TO_FURNACE;
					}
				}
				Utilities.CloseBank(this);
			}
			break;
		case WALK_TO_FURNACE:
			// 3199, 3499, 0
			log("WALK_TO_FURNACE state.");
			if (furnaceArea.contains(getLocalPlayer()))
				currentState = BotStates.USE_FURNACE;
			else
				Utilities.GoToArea(this, furnaceArea);
			break;
		case USE_FURNACE:
			log("USE_FURNACE state.");
			int[] randomNumbers = { 2, 3, 4, 5, 6, 7, 8, 9 };
			
			GameObject furnace = getGameObjects().closest("Furnace");
			Item goldBar = getInventory().get("Gold bar");
			
			goldBar.interact("Use");
			furnace.interact("Use");
			// sleep(1500);
			
			sleepUntil(() -> getWidgets().getWidgetChild(446, 8) != null, 3000);
			
			getWidgets().getWidgetChild(446, 8).interact("Make-X");
			sleep(500);
			int randomIndex = Calculations.random(randomNumbers.length);
			getKeyboard().type(String.valueOf(randomNumbers[randomIndex]), false);
			sleep(Calculations.random(30, 80));
			getKeyboard().type(String.valueOf(randomNumbers[randomIndex]), true);
			sleep(500);
			
			currentState = BotStates.WAIT_FOR_MAKING;
			
			break;
		case WAIT_FOR_MAKING:
			if (getInventory().contains("Sapphire") && getInventory().contains("Gold bar"))
			{
				currentState = BotStates.WAIT_FOR_MAKING;
			}
			else
			{
				currentState = BotStates.WALK_TO_BANK;
			}
			break;
		case BANK_MOULD_AND_RINGS:
			log("BANK_MOULD_AND_RINGS state.");
			getMouse().click();
			if (Utilities.OpenBank(this))
			{
				Utilities.DepositAll(this, "Ring mould");			
				Utilities.DepositAll(this, "Ring of recoil");
				currentState = BotStates.TAKE_RINGS;
			}
			break;
		case TAKE_RINGS:
			log("TAKE_RINGS state.");
			if (Utilities.OpenBank(this))
			{
				Utilities.Withdraw(this, "Sapphire ring", 27);
				Utilities.CloseBank(this);
				Utilities.OpenTab(this, Tab.MAGIC);
				currentState = BotStates.ENCHANT;
			}
			break;
		case ENCHANT:
			log("ENCHANT state.");
			if (getInventory().count("Sapphire ring") > 0)
			{
				if (getMagic().castSpell(Normal.LEVEL_1_ENCHANT))
				{
					sleep(500);
					if (getInventory().get("Sapphire ring").interact())
					{
						sleep(500);
						currentState = BotStates.ENCHANT;
					}
				}
			}
			else if (!getInventory().contains("Cosmic rune"))
			{
				sleep(11000);
				getTabs().logout();
				stop();
			}
			else
			{
				currentState = BotStates.BANK_MOULD_AND_RINGS;
			}
			break;
		}
		
		if (getDialogues().canContinue())
		{
			log("In dialogue");
			getDialogues().spaceToContinue();
			if (currentState == BotStates.WAIT_FOR_MAKING)
				currentState = BotStates.USE_FURNACE;
		}

		return Calculations.random(50, 100);
	}

	@Override
	public void onPaint(Graphics2D graphics)
	{
		if (canDraw)
			botGUI.DrawInGameGUI(graphics);
	}

	public boolean isStarted()
	{
		return isStarted;
	}
	
	public Timer getTimer()
	{
		return timer;
	}
}
