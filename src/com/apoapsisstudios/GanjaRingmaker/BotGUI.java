package com.apoapsisstudios.GanjaRingmaker;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.swing.JFrame;

import org.dreambot.api.script.AbstractScript;

public class BotGUI extends JFrame
{
	private static final long serialVersionUID = 1L;
	public final int WIDTH = 400;
	public final int HEIGHT = 250;
	private GanjaRingmakerMain context;
	
	private Image backgroundImage = null;

	public BotGUI(GanjaRingmakerMain context, String title)
	{
		super(title);
		this.context = context;
		setLayout(new FlowLayout(FlowLayout.CENTER));
		setSize(WIDTH, HEIGHT);
		setResizable(false);
		setLocationRelativeTo(null);
		try
		{
			InitializeComponents();
			backgroundImage = Utilities.LoadImage("http://i63.tinypic.com/2j48v94.png", 190, 111);
			
		}
		catch (IOException e)
		{
			AbstractScript.log(e.getMessage());
		}
	}
	
	private void InitializeComponents() throws MalformedURLException, IOException
	{		
		
	}

	public void Display()
	{
		setVisible(true);
	}

	public void Close()
	{
		setVisible(false);
	}
	
	public void DrawInGameGUI(Graphics2D graphics)
	{
		graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		graphics.drawImage(backgroundImage, 305, 347, null);
		graphics.setFont(new Font("Consolas", Font.PLAIN, 15));
		graphics.setColor(Color.BLACK);
		graphics.drawString("   Run Time:", 315, 365);
		graphics.drawString("      Empty:", 315, 382);
		graphics.drawString("      Empty:", 315, 399);
		graphics.drawString(context.getTimer().formatTime(), 420, 365);
	}
}
