package com.apoapsisstudios.GanjaRingmaker;

import java.awt.Image;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import org.dreambot.api.methods.map.Area;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.methods.tabs.Tab;
import org.dreambot.api.script.AbstractScript;
import org.dreambot.api.wrappers.interactive.GameObject;

public class Utilities
{
	private static boolean isBankOpen = false;
	
	public static Image LoadImage(String url, int width, int height)
	{
		Image infoImg = null;
		try
		{
			infoImg = ImageIO.read(new URL(url));
		}
		catch (IOException e)
		{
			AbstractScript.log(e.getMessage());
		}
		return infoImg.getScaledInstance(width, height, Image.SCALE_SMOOTH);
	}
	
	public static void GoToTile(AbstractScript context, Tile tile)
	{
		context.getWalking().walk(tile);
		AbstractScript.sleepUntil(() -> tile.distance(context.getLocalPlayer().getTile()) < 2, 3000);
	}
	
	public static void GoToArea(AbstractScript context, Area whichArea)
	{
		if (!context.getLocalPlayer().isMoving())
		{
			context.getWalking().walk(whichArea.getRandomTile());
			//AbstractScript.sleepUntil(() -> whichArea.contains(context.getLocalPlayer()), 3000);
			AbstractScript.sleepUntil(() -> !context.getLocalPlayer().isMoving(), 10000);
		}
	}
	
	public static boolean Withdraw(AbstractScript context, String item, int count)
	{
		boolean result = false;
		
		if (context.getBank().withdraw(item, count))
		{
			result = true;
			AbstractScript.sleepUntil(() -> context.getInventory().contains(item), 3000);
		}
		
		return result;
	}
	
	public static boolean OpenBank(AbstractScript context)
	{
		boolean result = false;
		OpenTab(context, Tab.INVENTORY);
		GameObject bankBooth = context.getGameObjects().closest(go -> go != null && go.getName().equals("Bank booth"));
		
		if (!isBankOpen && bankBooth != null && bankBooth.interact("Bank"))
		{
			if (AbstractScript.sleepUntil(() -> context.getBank().isOpen(), 9000))
			{
				result = true;
				isBankOpen = true;
			}
		}
		else if (isBankOpen)
		{
			result = true;
		}
		
		return result;
	}
	
	public static boolean CloseBank(AbstractScript context)
	{
		boolean result = false;
		
		if (isBankOpen)
		{
			if (context.getBank().close())
			{
				result = true;
				isBankOpen = false;
				AbstractScript.sleepUntil(() -> !context.getBank().isOpen(), 8000);
			}
		}
		
		return result;
	}
	
	public static boolean DepositAll(AbstractScript context, String itemName)
	{
		boolean result = false;
		
		if (context.getBank().depositAll(itemName))
		{
			result = true;
			AbstractScript.sleepUntil(() -> !context.getInventory().contains(itemName), 3000);
		}
		
		return result;
	}
	
	@Deprecated
	public static void BankAllItems(AbstractScript context, String itemName)
	{
		OpenTab(context, Tab.INVENTORY);
		GameObject bankBooth = context.getGameObjects().closest(npc -> npc != null && npc.getName().equals("Bank booth"));

		if (bankBooth != null && bankBooth.interact("Bank"))
		{
			if (AbstractScript.sleepUntil(() -> context.getBank().isOpen(), 9000))
			{
				context.getBank().depositAllExcept(item -> item.getName().contains("axe"));
				AbstractScript.sleepUntil(() -> !context.getInventory().contains("Logs"), 3000);
				if (context.getBank().close())
				{
					AbstractScript.sleepUntil(() -> !context.getBank().isOpen(), 8000);
				}
			}
		}
		AbstractScript.sleep(200);
	}
	
	public static void OpenTab(AbstractScript context, Tab tab)
	{
		if (!context.getTabs().isOpen(tab))
		{
			context.getTabs().open(tab);
			AbstractScript.sleepUntil(() -> context.getTabs().isOpen(tab), 2000);
		}
	}
}
